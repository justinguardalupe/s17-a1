let studentlist = [];

function addStudent(addStudent){
	studentlist.push(addStudent);
	console.log(addStudent	 + " was added to the student's list.");
}

function countStudents(){
	console.log("There are a total of " + studentlist.length + " students enrolled.");
}

function printStudents(){
	studentlist.sort();
	studentlist.forEach(function(list){
	console.log(list);
	});
}

function findStudent(findStudent) {

	let resultStudent = [];

	for(let i =0; i < studentlist.length; i++) {
		if(studentlist[i] == findStudent){
			resultStudent.push(studentlist[i]);
		}}
	if (resultStudent.length == 1){
		console.log(resultStudent[0] + " is an enrollee.");
	}

	if (resultStudent.length > 1){
		console.log(resultStudent.join(', ')+" are enrollees.");
	}

	if (resultStudent.length == 0){
		console.log("No student found with the name "+findStudent);
	}

}

function removeStudent(removeStudent) {


	for(let i =0; i < studentlist.length; i++) {

	if (studentlist[i] == removeStudent){
		delete studentlist[i];
		console.log(removeStudent+" was removed from the student's list.")
		break;
	}

} }

